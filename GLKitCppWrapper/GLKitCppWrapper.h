//
//  GLKitCppWrapper.h
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GLKitCppWrapper.
FOUNDATION_EXPORT double GLKitCppWrapperVersionNumber;

//! Project version string for GLKitCppWrapper.
FOUNDATION_EXPORT const unsigned char GLKitCppWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GLKitCppWrapper/PublicHeader.h>



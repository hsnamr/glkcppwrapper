//
//  GLUniform.m
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLUniform.h"

@implementation GLUniform

+(void)setMatrix3f:(GLKMatrix3)mat3 forUniform:(GLint)uniform {
    glUniformMatrix3fv(uniform, 1, GL_FALSE, mat3.m);
}

+(void)setMatrix4f:(GLKMatrix4)mat4 forUniform:(GLint)uniform {
    glUniformMatrix4fv(uniform, 1, GL_FALSE, mat4.m);
}

+(void)setFloat:(GLfloat)floatValue forUniform:(GLint)uniform {
    glUniform1f(uniform, floatValue);
}

+(void)setX:(GLfloat)x andY:(GLfloat)y forUniform:(GLint)uniform {
    GLfloat positionArray[2];
    positionArray[0] = x;
    positionArray[1] = y;
    
    glUniform2fv(uniform, 1, positionArray);
}

+(void)setWidth:(GLfloat)width andHeight:(GLfloat)height forUniform:(GLint)uniform {
    GLfloat sizeArray[2];
    sizeArray[0] = width;
    sizeArray[1] = height;
    
    glUniform2fv(uniform, 1, sizeArray);
}

+(void)setVec3:(GLKVector3)vec3 forUniform:(GLint)uniform {
    glUniform3fv(uniform, 1, vec3.v);
}

+(void)setVec4:(GLKVector4)vec4 forUniform:(GLint)uniform {
    glUniform4fv(uniform, 1, vec4.v);
}

+(void)setFloatArray:(GLfloat *)arrayValue length:(GLsizei)arrayLength forUniform:(GLint)uniform {
    glUniform1fv(uniform, arrayLength, arrayValue);
}

+(void)setInteger:(GLint)intValue forUniform:(GLint)uniform {
    glUniform1i(uniform, intValue);
}

+(int)getUniformIndex:(const GLchar*)uniformName forProgram:(GLuint)program {
    return glGetUniformLocation(program, uniformName);
}

@end

//
//  GLContext.m
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLContext.h"

@implementation GLContext

EAGLSharegroup* _sharegroup = nil;

-(instancetype)init {
    self = [super init];
    
    if(self) {
        self.context = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3 sharegroup:_sharegroup];
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
    }
    
    return self;
}

-(instancetype)initWithSharegroup:(EAGLSharegroup *)sharegroup {
    self = [super init];
    
    if(self) {
        self.context = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3 sharegroup:sharegroup];
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
    }
    
    return self;
}

+(GLContext *)sharedContext {
    static dispatch_once_t pred;
    static GLContext *sharedContext = nil;
    
    dispatch_once(&pred, ^{
        sharedContext = [[[self class] alloc] init];
    });
    return sharedContext;
}

-(void)makeCurrentContext {
    if([EAGLContext currentContext] != self.context) {
        [EAGLContext setCurrentContext:self.context];
    }
}

-(EAGLSharegroup*)getSharegroup {
    if(_sharegroup) {
        return _sharegroup;
    }
    return self.context.sharegroup;
}

@end

//
//  GLController.m
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLController.h"

@interface GLController () {
    GLuint _program;
    GLuint _vertexShader;
    GLuint _fragmentShader;
    const GLchar* _vshSource;
    const GLchar* _fshSource;
    GLRawIO* _glInOut;
    GLShader* _glSh;
    EAGLContext* _context;
    int _rows;
    int _cols;
}

@end

@implementation GLController

- (void)setupContext {
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
}

-(instancetype)init {
    self = [super init];
    
    if(self) {
        [self setupContext];
        _vshSource = NULL;
        _fshSource = NULL;
        _rows = 0;
        _cols = 0;
    }
    
    return self;
}

-(void)loadShader {
    if(NULL == _vshSource || NULL == _fshSource) {
        NSLog(@"Error: Call setVertexShader:(const GLchar*)vshSource before calling loadShader\n");
        return;
    }
    if(NULL == _fshSource) {
        NSLog(@"Error: Call setFragmentShader:(const GLchar*)fshSource before calling loadShader\n");
        return;
    }
    _glSh = [[GLShader alloc]initWithVertex:_vshSource andFragmentShader:_fshSource];
    _vertexShader = _glSh.vertHandler;
    _fragmentShader = _glSh.fragHandler;
    _program = _glSh.program;
}

#pragma mark Data methods
-(void)uploadData:(unsigned char*)data rows:(int)rows cols:(int)cols {
    if(!_glInOut) {
        _glInOut = [GLRawIO new];
    }
    _rows = rows;
    _cols = cols;
    [_glInOut uploadBytes:data width:cols height:rows];
}

-(unsigned char*)downloadData {
    if(!_glInOut) {
        _glInOut = [GLRawIO new];
    }
    if(!_rows || !_cols) {
        NSLog(@"Nothing to download,\nuploadData:(unsigned char*)data rows:(int)rows cols:(int)cols must be called before downloadData\n");
    }
    unsigned char* data = (unsigned char*)malloc(sizeof(unsigned char*)*_cols*_rows);
    [_glInOut downloadBytes:data width:_cols height:_rows];
    return data;
}

#pragma mark Getters
-(unsigned int)getProgram {
    return _program;
}

-(GLuint)getVertexShaderHandle {
    return _vertexShader;
}

-(GLuint)getFragmentShaderHandle {
    return _fragmentShader;
}

-(GLuint)getUniformIndex:(const GLchar *)uniformName forProgram:(GLuint)program {
    return [GLUniform getUniformIndex:uniformName forProgram:program];
}

#pragma mark Setters
-(void)setVertexShader:(const GLchar*)vshSource {
    if(NULL == vshSource) {
        NSLog(@"Error: Vertex shader string cannot be NULL\n");
        return;
    }
    _vshSource = vshSource;
}

-(void)setFragmentShader:(const GLchar*)fshSource {
    if(NULL == fshSource) {
        NSLog(@"Error: Fragment shader string cannot be NULL\n");
        return;
    }
    _fshSource = fshSource;
}

-(void)setFloat:(GLfloat)floatValue forUniform:(GLint)uniform {
    [GLUniform setFloat:floatValue forUniform:uniform];
}

@end

//
//  GLInOut.m
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLRawIO.h"
#import "GLContext.h"

@interface GLRawIO () {
    GLuint _texture;
}

@end

@implementation GLRawIO

-(void)setTexture:(GLuint)texture {
    _texture = texture;
}

-(GLuint)getTexture {
    return _texture;
}

-(void)uploadBytes:(GLubyte *)bytes width:(GLsizei)width height:(GLsizei)height {
    [GLContext sharedContext];
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _texture);
    // internal format GL_R8 is 8bit red, should be okay for grayscale
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, bytes);
//    glBindTexture(GL_TEXTURE_2D, 0)
}

-(void)downloadBytes:(GLubyte*)bytes width:(GLsizei)width height:(GLsizei)height {
    [GLContext sharedContext];
    
    // GL_LUMINANCE is grayscale without alpha
    glReadPixels(0, 0, width, height, GL_LUMINANCE, GL_UNSIGNED_BYTE, bytes);
}


@end

//
//  GLUniform.h
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <GLKit/GLKit.h>

@interface GLUniform : NSObject

+(void)setMatrix3f:(GLKMatrix3)mat3 forUniform:(GLint)uniform;
+(void)setMatrix4f:(GLKMatrix4)mat4 forUniform:(GLint)uniform;

+(void)setFloat:(GLfloat)floatValue forUniform:(GLint)uniform;

+(void)setX:(GLfloat)x andY:(GLfloat)y forUniform:(GLint)uniform;
+(void)setWidth:(GLfloat)width andHeight:(GLfloat)height forUniform:(GLint)uniform;

+(void)setVec3:(GLKVector3)vec3 forUniform:(GLint)uniform;
+(void)setVec4:(GLKVector4)vec4 forUniform:(GLint)uniform;

+(void)setFloatArray:(GLfloat *)arrayValue length:(GLsizei)arrayLength forUniform:(GLint)uniform;
+(void)setInteger:(GLint)intValue forUniform:(GLint)uniform;

+(int)getUniformIndex:(const GLchar*)uniformName forProgram:(GLuint)program;

@end

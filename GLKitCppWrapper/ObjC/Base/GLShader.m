//
//  GLShader.m
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLShader.h"

@implementation GLShader

-(instancetype)initWithVertex:(const GLchar *)vsh andFragmentShader:(const GLchar *)fsh {
    self = [super init];
    
    if(self) {
        _program = glCreateProgram();
        
        // Create and compile vertex shader.
        if (![GLShader compileShader:&_vertHandler type:GL_VERTEX_SHADER source:vsh]) {
            NSLog(@"Failed to compile vertex shader");
        }
        
        // Create and compile fragment shader.
        if (![GLShader compileShader:&_fragHandler type:GL_FRAGMENT_SHADER source:fsh]) {
            NSLog(@"Failed to compile fragment shader");
        }
        
        // Attach vertex shader to program.
        glAttachShader(_program, _vertHandler);
        
        // Attach fragment shader to program.
        glAttachShader(_program, _fragHandler);
        
        [GLShader linkProgram:_program];
    }
    
    return self;
}

+(BOOL)compileShader:(GLuint *)shader type:(GLenum)type source:(const GLchar *)source {
    GLint status;
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

+(BOOL)linkProgram:(GLuint)program {
    GLint status;
    glLinkProgram(program);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(program, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end

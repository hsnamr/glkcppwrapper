//
//  GLRawIO.h
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

/*
 This class is used to write original image to GPU and read process image from GPU.
 Note:  on iOS devices the CPU and GPU share the memory, so there is no need to copy,
        we only need to bind the image data to OpenGL's buffer.
  */

#import <Foundation/Foundation.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <GLKit/GLKit.h>

@interface GLRawIO : NSObject

-(void)setTexture:(GLuint)texture;
-(GLuint)getTexture;

// writes image data (bytes) to GPU memory
-(void)uploadBytes:(GLubyte *)bytes width:(GLsizei)width height:(GLsizei)height;

// reads image data (bytes) from GPU memory
-(void)downloadBytes:(GLubyte*)bytes width:(GLsizei)width height:(GLsizei)height;

@end

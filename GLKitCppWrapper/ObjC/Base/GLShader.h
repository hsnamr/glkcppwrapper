//
//  GLShader.h
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

/*
 This class is used to compile the shaders
 */

#import <Foundation/Foundation.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <GLKit/GLKit.h>

@interface GLShader : NSObject

@property GLuint program;
@property GLuint vertHandler;
@property GLuint fragHandler;

-(instancetype)initWithVertex:(const GLchar*)vsh andFragmentShader:(const GLchar*)fsh;
+(BOOL)compileShader:(GLuint *)shader type:(GLenum)type source:(const GLchar *)source;
+(BOOL)linkProgram:(GLuint)program;

@end

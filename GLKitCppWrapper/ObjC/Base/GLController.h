//
//  GLController.h
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

/*
 This class is the entry point
 */

#import <Foundation/Foundation.h>
#import "GLContext.h"
#import "GLRawIO.h"
#import "GLShader.h"
#import "GLUniform.h"

@interface GLController : NSObject

-(void)setVertexShader:(const GLchar*)vshSource;
-(void)setFragmentShader:(const GLchar*)fshSource;

-(void)loadShader;

-(unsigned int)getProgram;
-(GLuint)getVertexShaderHandle;
-(GLuint)getFragmentShaderHandle;

-(void)uploadData:(unsigned char*)data rows:(int)rows cols:(int)cols;
-(unsigned char*)downloadData;

-(void)setFloat:(GLfloat)floatValue forUniform:(GLint)uniform;
-(GLuint)getUniformIndex:(const GLchar*)uniformName forProgram:(GLuint)program;

@end

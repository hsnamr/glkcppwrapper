//
//  GLContext.h
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <GLKit/GLKit.h>

@interface GLContext : NSObject

@property(strong, nonatomic)EAGLContext* context;

-(instancetype)init;
-(instancetype)initWithSharegroup:(EAGLSharegroup*)sharegroup;

+(GLContext *)sharedContext;

-(void)makeCurrentContext;
-(EAGLSharegroup*)getSharegroup;

@end

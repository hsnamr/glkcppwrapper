//
//  GLOperation.m
//  GLKitCppWrapper
//
//  Created by Hussian Ali AL-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import "GLOperation.h"

@implementation GLOperation

-(instancetype)initWithVertexShader:(const char*)vsh fragmentShader:(const char*)fsh {
    self = [super init];
    
    if(self) {
        GLController* glC = [GLController new];
        
        [glC setVertexShader:vsh];
        [glC setFragmentShader:fsh];
        
        [glC loadShader];
        
        self.shaderProgram = [glC getProgram];
        self.vertextShader = [glC getVertexShaderHandle];
        self.fragmentShader = [glC getFragmentShaderHandle];
    }
    
    return self;
}

-(void)setupFilterForSize {
    glUniform1f(self.texelWidthUniform, texelWidth);
    glUniform1f(self.texelHeightUniform, texelHeight);
}

- (void)setupFilterForSize:(CGSize)filterFrameSize {
    glUniform1f(self.texelWidthUniform, texelWidth);
    glUniform1f(self.texelHeightUniform, texelHeight);
}

- (void)setTexelWidth:(GLfloat)newValue {
    texelWidth = newValue;
    [GLUniform setFloat:texelWidth forUniform:self.texelWidthUniform];
}

-(void)setTexelHeight:(GLfloat)newValue {
    texelHeight = newValue;
    [GLUniform setFloat:texelHeight forUniform:self.texelHeightUniform];
}

-(float)getTexelWidth {
    return texelWidth;
}

-(float)getTexelHeight {
    return texelHeight;
}

@end

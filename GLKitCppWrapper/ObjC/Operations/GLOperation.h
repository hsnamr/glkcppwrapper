//
//  GLOperation.h
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLController.h"

@interface GLOperation : NSObject {
    float texelWidth, texelHeight;
    BOOL hasOverriddenImageSizeFactor;
}

@property GLuint inputsCount;
@property GLuint shaderProgram;
@property GLint vertextShader;
@property GLint fragmentShader;
@property GLint texelWidthUniform;
@property GLint texelHeightUniform;


-(instancetype)initWithVertexShader:(const char*)vsh fragmentShader:(const char*)fsh;
-(void)setTexelWidth:(GLfloat)newValue;
-(void)setTexelHeight:(GLfloat)newValue;

@end

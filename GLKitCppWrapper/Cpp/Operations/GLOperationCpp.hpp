//
//  GLOperationCpp.hpp
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/22/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#ifndef GLOperationCpp_hpp
#define GLOperationCpp_hpp

#include "GLWrapperCommon.hpp"

OBJC_CLASS(GLOperation);

namespace glWrapper {
    class GLOperationCpp {
        GLOperation* wrapped;
    public:
        GLOperationCpp(const char* vsh, const char* fsh);
        ~GLOperationCpp();
        
        void setTexelWidth(GLfloat newValue);
        void setTexelHeight(GLfloat newValue);
    };
}

#endif /* GLBasicOperationCpp_hpp */

//
//  GLBasicOperationCpp.cpp
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/22/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#include "GLOperationCpp.hpp"
#import "GLOperation.h"

namespace glWrapper {
    GLOperationCpp::GLOperationCpp(const char* vsh, const char* fsh) :
    wrapped([[GLOperation alloc] initWithVertexShader:vsh fragmentShader:fsh])
    {
    }
    
    GLOperationCpp::~GLOperationCpp()
    {
        [wrapped release];
    }
    
    void GLOperationCpp::setTexelWidth(GLfloat newValue) {
        [wrapped setTexelWidth:newValue];
    }
    
    void GLOperationCpp::setTexelHeight(GLfloat newValue) {
        [wrapped setTexelHeight:newValue];
    }
}

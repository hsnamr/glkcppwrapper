//
//  GLWrapper.hpp
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#ifndef GLWrapper_hpp
#define GLWrapper_hpp

#include "GLWrapperCommon.hpp"

OBJC_CLASS(GLController);

#define EAGL 1

namespace glWrapper {
    class GLControllerCpp {
        GLController* wrapped;
    public:
        GLControllerCpp();
        ~GLControllerCpp();
        
        void loadShader();
        
        void setVertexShader(const GLchar* vshSource);
        void setFragmentShader(const GLchar* fshSource);
        
        unsigned int getVertexShaderHandle();
        unsigned int getFragmentShaderHandle();
        
        void uploadData(unsigned char* data,int rows, int cols);
        unsigned char* downloadData();
        
        void setFloat(GLfloat v, GLint uniform);
        GLuint getUniformIndex(const GLchar* uniformName, GLuint program);
    };
}

#endif /* GLWrapper_hpp */

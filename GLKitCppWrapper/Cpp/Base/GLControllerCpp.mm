//
//  GLWrapper.cpp
//  HealthX
//
//  Created by Hussian Ali Al-Amri on 1/19/17.
//  Copyright © 2017 H4n. All rights reserved.
//

// compile with -fno-objc-arc

#include "GLControllerCpp.hpp"
#import "GLController.h"

namespace glWrapper {
    GLControllerCpp::GLControllerCpp() :
    wrapped([[GLController alloc] init]) { }
    
    GLControllerCpp::~GLControllerCpp() {
        [wrapped release];
    }
    
    void GLControllerCpp::loadShader() {
        [wrapped loadShader];
    }
    
    void GLControllerCpp::setVertexShader(const GLchar* vshSource) {
        [wrapped setVertexShader:vshSource];
    }
    
    void GLControllerCpp::setFragmentShader(const GLchar* fshSource) {
        [wrapped setFragmentShader:fshSource];
    }
    
    unsigned int GLControllerCpp::getVertexShaderHandle() {
        return [wrapped getVertexShaderHandle];
    }
    
    unsigned int GLControllerCpp::getFragmentShaderHandle() {
        return [wrapped getFragmentShaderHandle];
    }
    
    void GLControllerCpp::uploadData(unsigned char* data,int rows, int cols) {
        [wrapped uploadData:data rows: rows cols: cols];
    }
    
    unsigned char* GLControllerCpp::downloadData() {
        return [wrapped downloadData];
    }
    
    void GLControllerCpp::setFloat(GLfloat v, GLint uniform) {
        [wrapped setFloat:v forUniform:uniform];
    }
    
    unsigned int GLControllerCpp::getUniformIndex(const GLchar* uniformName, GLuint program) {
        return [wrapped getUniformIndex:uniformName forProgram:program];
    }
}

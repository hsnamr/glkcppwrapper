//
//  GLContextCpp.cpp
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/22/17.
//  Copyright © 2017 H4n. All rights reserved.
//

// compile with -fno-objc-arc

#include "GLContextCpp.hpp"
#import "GLContext.h"

namespace glWrapper {
    GLContextCpp::GLContextCpp() :
    wrapped([[GLContext alloc] init]) { }
    
    GLContextCpp::GLContextCpp(EAGLSharegroup* sharegroup) :
    wrapped([[GLContext alloc] initWithSharegroup:sharegroup]) { }
    
    GLContextCpp::~GLContextCpp() {
        [wrapped release];
    }
    
    GLContext* GLContextCpp::sharedContext() {
        return [GLContext sharedContext];
    }
    
    void GLContextCpp::makeCurrentContext() {
        [wrapped makeCurrentContext];
    }
    
    EAGLSharegroup* GLContextCpp::getSharegroup() {
        return [wrapped getSharegroup];
    }
}

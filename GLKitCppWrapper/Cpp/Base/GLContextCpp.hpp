//
//  GLContextCpp.hpp
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/22/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#ifndef GLContextCpp_hpp
#define GLContextCpp_hpp

#include "GLWrapperCommon.hpp"

OBJC_CLASS(GLContext);
OBJC_CLASS(EAGLSharegroup);

namespace glWrapper {
    class GLContextCpp {
        GLContext* wrapped;
    public:
        GLContextCpp();
        GLContextCpp(EAGLSharegroup* sharegroup);
        ~GLContextCpp();
        GLContext* sharedContext();
        void makeCurrentContext();
        EAGLSharegroup* getSharegroup();
    };
}

#endif /* GLContextCpp_hpp */

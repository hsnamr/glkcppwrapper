//
//  GLWrapperCommon.hpp
//  GLKitCppWrapper
//
//  Created by Hussian Ali Al-Amri on 1/20/17.
//  Copyright © 2017 H4n. All rights reserved.
//

#ifndef GLWrapperCommon_hpp
#define GLWrapperCommon_hpp

#include <stdio.h>
#include <objc/objc-runtime.h>
#include <GLKit/GLKMathTypes.h>
#include <OpenGLES/gltypes.h>

#ifdef __OBJC__
#define OBJC_CLASS(name) @class name
#else
#define OBJC_CLASS(name) typedef struct objc_object name
#endif

#endif /* GLWrapperCommon_h */

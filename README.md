A simple wrapper that allows using OpenGL ES on iOS from C++.

Initially written to add OpenGL support to OpenCV on iOS.

This project is not affiliated with OpenCV.